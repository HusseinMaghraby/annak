<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $guarded = [];

    public function getImageAttribute($image){

        return asset('images/'.$image);
    }
}
