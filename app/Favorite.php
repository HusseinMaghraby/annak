<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    //
    protected $guarded = [];

    public function store(){
        return $this->belongsTo('App\Store', 'store_id');
    }

    public function product(){
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

}
