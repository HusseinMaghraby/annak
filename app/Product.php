<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $guarded = [];

    public function getImageAttribute($image){

        return asset('images/'.$image);
    }



    public function store(){
        return $this->belongsTo('App\Store', 'store_id');
    }

    public function storeCategory(){
        return $this->belongsTo('App\StoreCategory', 'storeCategory_id');
    }
}
