<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Driver extends Authenticatable
{
    //
    protected $guarded = [];

    public function setPasswordAttribute($password){

        if(!empty($password)){

            $this->attributes['password'] = bcrypt($password);
        }
    }
    public function getImageAttribute($image){

        return asset('images/'.$image);
    }

    public function getLicenseAttribute($image){

        return asset('images/'.$image);
    }

    public function getPersonalAttribute($image){

        return asset('images/'.$image);
    }

    public function getIdAttribute($image){

        return asset('images/'.$image);
    }

    public function city(){
        return $this->belongsTo('App\City', 'city_id');
    }
}
