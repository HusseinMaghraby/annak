<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Store extends Authenticatable
{
    //
    protected $guarded = [];

    public function setPasswordAttribute($password){

        if(!empty($password)){

            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function images(){
        return $this->hasMany('App\StoreImage', 'store_id');
    }

    public function license(){
        return $this->hasMany('App\StoreLicense', 'store_id');
    }

    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function city(){

        return $this->belongsTo('App\City', 'city_id');
    }

    public function region(){

        return $this->belongsTo('App\Region', 'region_id');
    }

    public function getImageAttribute($image){

        return asset('images/'.$image);
    }

}
