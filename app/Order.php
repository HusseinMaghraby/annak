<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $guarded = [];

    public function getImageAttribute($image){
        return asset('images/' . $image);
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
