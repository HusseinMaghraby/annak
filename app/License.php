<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    //
    protected $guarded = [];

    public function getImageAttribute($image){

        return asset('images/'.$image);
    }

    public function Store(){
        return $this->belongsTo('App\Store', 'store_id');
    }

}
