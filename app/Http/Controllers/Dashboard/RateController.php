<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use App\Rate;
use App\StoreType;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class RateController extends Controller
{
    private $resources = 'rates';
    private $resource = [
        'route' => 'admin.rates',
        'view' => "rates",
        'icon' => "star-half-o",
        'title' => "RATES",
        'action' => "",
        'header' => "Rates"
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $data = Rate::orderBy('id', 'DESC')->paginate(10);
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.index',compact('data', 'resource'));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        Rate::findOrFail($id)->delete();
        App::setLocale($lang);
//        flashy(__('dashboard.deleted'));
//        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    public function multiDelete($lang)
    {
        foreach (\request('checked') as $id)
        {
            Rate::findOrFail($id)->delete();
        }
        App::setLocale($lang);
        flashy(__('dashboard.deleted'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

}
