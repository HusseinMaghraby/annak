<?php

namespace App\Http\Controllers\Dashboard;

use App\Product;
use App\Image;
use App\Http\Controllers\Controller;
use App\Store;
use Illuminate\Http\Request;
use Validator;
use Storage;
use Auth;

class ImageController extends Controller
{
    private $resources = 'images';
    private $resource = [
        'route' => 'admin.images',
        'view' => "images",
        'icon' => "picture-o",
        'title' => "STORES",
        'action' => "",
        'header' => "Images",
        'return' => "admin.stores",
        'index' => "show",
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $id)
    {
        $name = Store::find($id)->name_ar;
        $data = Image::Where('store_id', $id)->orderBy('id', 'desc')->get();
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.index',compact('data', 'resource', 'name', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang, $id)
    {
        $item = Store::find($id);
        $name = $item->name_ar;
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.create',compact( 'resource', 'name', 'id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $lang, $id)
    {
        $rules =  [
            'image' => 'required|mimes:png,jpg,jpeg,gif',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }

        $item = Image::create([
            'store_id' => $id,
        ]);
        if ($request->image)
        {
            $file =$request->image;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $item->image = $name;
            $item->save();
        }

        flashy(__('dashboard.created'));
        return redirect()->route($this->resource['route'].'.'.$this->resource['index'], [$lang, $id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $item = Store::find(Image::find($id)->store_id);
        $name = $item->name_ar;
        $ids = $item->id;
        $item = Image::find($id);
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.edit',compact('item', 'resource', 'name', 'ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $ids = Image::find($id)->store_id;
        $item = Image::find($id);
        $rules =  [
            'image' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }

        if( $request->image){
            $path = parse_url($item->image);
            unlink(public_path($path['path']));

            $file =$request->image;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $item->image = $name;
            $item->save();
        }

        flashy(__('dashboard.updated'));
        return redirect()->route($this->resource['route'].'.'.$this->resource['index'], [$lang, $ids]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        $item = Image::findOrFail($id);
        $path = parse_url($item->image);
        unlink(public_path($path['path']));
        $item->delete();
        return response()->json(true);
    }

    public function multiDelete($lang)
    {
        foreach (\request('checked') as $id)
        {
            $ids = Image::find($id)->store_id;
            $item = Image::findOrFail($id);
            $path = parse_url($item->image);
            unlink(public_path($path['path']));
            $item->delete();
        }

        flashy(__('dashboard.deleted'));
        return redirect()->route($this->resource['route'].'.'.$this->resource['index'], [$lang, $ids]);
    }

}
