<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class CategoryController extends Controller
{
    private $resources = 'categories';
    private $resource = [
        'route' => 'admin.categories',
        'view' => "categories",
        'icon' => "bookmark",
        'title' => "CATEGORIES",
        'action' => "",
        'header' => "Categories"
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $data = Category::orderBy('id', 'DESC')->paginate(10);
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.index',compact('data', 'resource'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $resource = $this->resource;
        $resource['action'] = 'Create';
        return view('dashboard.views.'.$this->resources.'.create',compact( 'resource'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $lang)
    {
        $rules =  [
            'name_ar' => 'required',
//            'name_en' => 'required',
            'image' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }

        $input = $request->except('image');
        $file =$request->image;
        $name = time() . $file->getClientOriginalName();
        $file->move('images', $name);
        $input['image'] = $name;

        Category::create($input);
        App::setLocale($lang);
        flashy(__('dashboard.created'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $id)
    {
        $data = SubCategory::where('category_id', $id)->paginate(10);
        $resource = $this->resource;
        $resource['action'] = 'Show';
        return view('dashboard.views.'.$this->resources.'.show',compact('data', 'resource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $resource = $this->resource;
        $resource['action'] = 'Edit';
        $item = Category::findOrFail($id);
        return view('dashboard.views.' .$this->resources. '.edit', compact('item', 'resource'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $rules =  [
            'name_ar' => 'required',
//            'name_en' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }


        $input = $request->except('image');
        if( $request->image){
            $category = Category::findOrFail($id);
            $path = parse_url($category->image);
            public_path($path['path']);

            $file =$request->image;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $input['image'] = $name;
        }

        Category::find($id)->update($input);

        App::setLocale($lang);
        flashy(__('dashboard.updated'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
        if(! SubCategory::Where('category_id', $id)->first()){

            $item = Category::findOrFail($id);
            unlink(str_replace( asset(''), '', $item->image));
            $item->delete();
            App::setLocale($lang);
        }
        flashy(__('dashboard.deleted'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    public function multiDelete($lang)
    {


        foreach (\request('checked') as $id)
        {
            if(! SubCategory::Where('category_id', $id)->first()){
                $item = Category::findOrFail($id);
                unlink(str_replace( asset(''), '', $item->image));
                $item->delete();
            }
        }
        App::setLocale($lang);

        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    public function search(Request $request, $lang)
    {
        $resource = $this->resource;
        $data = Category::where('name_ar', 'LIKE', '%'.$request->text.'%')
            ->orWhere('name_en', 'LIKE', '%'.$request->text.'%')
            ->paginate(10);
        App::setLocale($lang);
        return view('dashboard.views.' .$this->resources. '.index', compact('data', 'resource'));
    }
}
