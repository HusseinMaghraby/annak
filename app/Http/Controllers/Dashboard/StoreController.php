<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use App\Image;
use App\License;
use App\Store;
use App\StoreType;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class StoreController extends Controller
{
    private $resources = 'stores';
    private $resource = [
        'route' => 'admin.stores',
        'view' => "stores",
        'icon' => "flag",
        'title' => "STORES",
        'action' => "",
        'header' => "Stores"
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $data = Store::orderBy('id', 'DESC')->paginate(10);
        $resource = $this->resource;
        return view('dashboard.views.'.$this->resources.'.index',compact('data', 'resource'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang)
    {
        $resource = $this->resource;
        $resource['action'] = 'Create';
        $categories = Category::select("name_$lang as name", 'id')->get();
        $cities = City::select("name_$lang as name", 'id')->get();
        return view('dashboard.views.'.$this->resources.'.create',compact( 'resource','categories','cities'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $lang)
    {
        $rules =  [

            'category_id' => 'required',
            'city_id' => 'required',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'email' => 'required',
            'password' => 'required',
            'image' => 'required',
            'images' => 'required',
            'licenses' => 'required',
            'phone' => 'required',
            'bank' => 'required',
            'account' => 'required',
            'address_ar' => 'required',
//            'address_en' => 'required',
            'minimum' => 'required',
            'active' => 'required',
            'status' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }

        $input = $request->except('image', 'images', 'licenses');
        $file =$request->image;
        $name = time() . $file->getClientOriginalName();
        $file->move('images', $name);
        $input['image'] = $name;

        $store = Store::create($input);

        if( $request->images){
            foreach ($request->images as $image) {
                $file = $image;
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                Image::create([
                    'image' => $name,
                    'store_id' => $store->id,
                ]);
            }

        }

        if( $request->licenses){
            foreach ($request->licenses as $image) {
                $file = $image;
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                License::create([
                    'image' => $name,
                    'store_id' => $store->id,
                ]);
            }

        }

        App::setLocale($lang);
        flashy(__('dashboard.created'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $id)
    {
        $item = Store::where('id', $id)->first();
        $resource = $this->resource;
        $resource['action'] = 'Show';
        return view('dashboard.views.'.$this->resources.'.show',compact('item', 'resource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($lang, $id)
    {
        $resource = $this->resource;
        $resource['action'] = 'Edit';
        $item = Store::findOrFail($id);
        $categories = Category::select("name_$lang as name", 'id')->get();
        $cities = City::select("name_$lang as name", 'id')->get();

        return view('dashboard.views.' .$this->resources. '.edit', compact('item', 'resource', 'categories', 'cities'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang, $id)
    {
        $rules =  [

            'category_id' => 'required',
            'city_id' => 'required',
            'name_ar' => 'required',
//            'name_en' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'bank' => 'required',
            'account' => 'required',
            'address_ar' => 'required',
//            'address_en' => 'required',
            'minimum' => 'required',
            'active' => 'required',
            'status' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            flashy()->error($validator->errors()->all()[0]);
            return back();
        }
        $store = Store::findOrFail($id);
        $input = $request->except('image', 'images', 'licenses');
        if( $request->image){
            unlink(str_replace( asset(''), '', $store->image));
            $file =$request->image;
            $name = time() . $file->getClientOriginalName();
            $file->move('images', $name);
            $input['image'] = $name;
        }

        if( $request->images){

            foreach ($request->images as $image) {
                $file = $image;
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                Image::create([
                    'image' => $name,
                    'store_id' => $id,
                ]);
            }
        }

        if( $request->licenses){
            foreach ($request->licenses as $image) {
                $file = $image;
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                License::create([
                    'image' => $name,
                    'store_id' => $id,
                ]);
            }
        }

        Store::find($id)->update($input);

        App::setLocale($lang);
        flashy(__('dashboard.updated'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($lang, $id)
    {
       $item =  Store::findOrFail($id);
        $path = parse_url($item->image);
        unlink(public_path($path['path']));
        $item->delete();
        App::setLocale($lang);
//        flashy(__('dashboard.deleted'));
//        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    public function multiDelete($lang)
    {
        foreach (\request('checked') as $id)
        {
            $item =  Store::findOrFail($id);
            $path = parse_url($item->image);
            unlink(public_path($path['path']));
            $item->delete();
        }
        App::setLocale($lang);
        flashy(__('dashboard.deleted'));
        return redirect()->route($this->resource['route'].'.index', $lang);
    }

    public function search(Request $request, $lang)
    {
        $resource = $this->resource;
        $data = Store::where('name_ar', 'LIKE', '%'.$request->text.'%')
            ->orWhere('name_en', 'LIKE', '%'.$request->text.'%')
            ->paginate(10);
        App::setLocale($lang);
        return view('dashboard.views.' .$this->resources. '.index', compact('data', 'resource'));
    }
}
