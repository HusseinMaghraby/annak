<?php

namespace App\Http\Controllers\Dashboard;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Rate;
use App\Region;
use App\Setting;
use App\Store;
use App\Suggestion;
use App\User;
use App\Category;
use App\City;

class HomeController extends Controller
{
    private $resource = [
        'route' => 'admin.home',
        'icon' => "home",
        'title' => "DASHBOARD",
        'action' => "",
        'header' => "home"
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statistics = [
            'users'          => User::count(),
            'admins'          => Admin::count(),
            'banners'          => Banner::count(),
            'categories'     => Category::count(),
            'cities'         => City::count(),
            'rates'         => Rate::count(),
            'store'         => Store::count(),
            'regions'         => Region::count(),
            'suggestions'         => Suggestion::count(),
            'settings'         => Setting::count(),
        ];
        $resource = $this->resource;

        return view('dashboard.home', compact('statistics', 'resource'));
    }
}
