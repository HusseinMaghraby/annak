<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Cart;
use App\Driver;
use App\Offer;
use App\Order;
use App\OrderCart;
use App\Rate;
use App\Setting;
use App\Store;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['current'] = Order::whereIn('status', [1 , 2, 3, 4, 5, 6, 7, 8])->where('user_id', $auth)->get();

        $data['current']->map(function ($item)
        {
            $item->status = (int)$item->status;
            if ($item->payment) $item->payment = (int)$item->payment;
            $item->type = (int)$item->type;
            $item->total = $item->cost + $item->vat;
            $item->date = date('Y-m-d h:i A', strtotime($item->created_at));
            $item->offer = Offer::select('id', 'cost', 'time', 'driver_id')->where('status', 2)
                                    ->where('order_id', $item->id)
                                    ->with('Driver:id,name,image,average_rate')->first();

            unset($item->user_id);
            unset($item->created_at);
            unset($item->updated_at);
            if ($item->offer) unset($item->offer->driver_id);
        });

        $data['previous'] = Order::whereIn('status', [9 , 10])->where('user_id', $auth)->get();

        $data['previous']->map(function ($item)
        {
            $item->status = (int)$item->status;
            if ($item->payment) $item->payment = (int)$item->payment;
            $item->type = (int)$item->type;
            $item->total = $item->cost + $item->vat;
            $item->date = date('Y-m-d h:i A', strtotime($item->created_at));
            $item->offer = Offer::select('id', 'cost', 'time', 'driver_id')->where('status', 2)
                                    ->where('order_id', $item->id)
                                    ->with('Driver:id,name,image,average_rate')->first();

            unset($item->user_id);
            unset($item->created_at);
            unset($item->updated_at);
            if ($item->offer)  unset($item->offer->driver_id);
        });

        return $this->successResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'delivery_longitude'  => 'required',
            'delivery_latitude'  => 'required',
            'type'  => 'required|in:1,2',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (request('type') == 1)
        {
            $carts = Cart::where('user_id', $auth)->where('status', 1)->get();

            $inputs = request()->all();
            $inputs['cost'] = $carts->sum('price');
            $inputs['vat'] = Setting::first()->vat;
            $inputs['user_id'] = $auth;

            $order = Order::create($inputs);
            foreach ($carts as $cart)
            {
                $cart->update(['status' => 2]);
                OrderCart::create([
                    'order_id' => $order->id,
                    'cart_id' => $cart->id,
                    'store_id' => $cart->Product->Store->id,
                ]);
            }

            return $this->successResponse(null, __('api.OrderCreated'));
        }
        else
        {
            $rules =  [
                'payment'  => 'required|in:1,2,3',
                'order_longitude'  => 'required',
                'order_latitude'  => 'required',
                'desc'  => 'required',
                'expected_cost'  => 'required',
                'image'  => 'nullable',
            ];

            $validator = Validator::make(request()->all(), $rules);
            $errors = $this->formatErrors($validator->errors());
            if($validator->fails()) {return $this->errorResponse($errors);}

            $inputs = request()->except('image');
            $inputs['user_id'] = $auth;
            $inputs['vat'] = Setting::first()->vat;

            if (request('image'))
            {
                $inputs['image'] = $this->uploadBase64(request('image'), 'orders');
            }

            Order::create($inputs);
            return $this->successResponse(null, __('api.OrderCreated'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['order'] = Order::find($id);

        $data['order']->status = (int)$data['order']->status;
        if ($data['order']->payment)$data['order']->payment = (int)$data['order']->payment;
        $data['order']->type = (int)$data['order']->type;
        $data['order']->date = date('Y-m-d h:i A', strtotime($data['order']->created_at));
        $data['order']->products = Cart::whereIn('id', $data['order']->Cart()->select('cart_id')->get())
            ->select('id', 'quantity', 'price', 'product_id')->get();

        $data['order']->products->map(function ($item) use ($lang)
        {
            $item->name = $item->Product["name_$lang"];
            $item->image = $item->Product->image;
            $item->store = $item->Product->Store()->select('id', "name_$lang as name")->first();

            unset($item->product_id);
            unset($item->Product);
        });

        $data['order']->rate = Rate::where('order_id', $id)->where('user_id', $auth)->where('type', 2)
                                        ->select('id', 'comment', 'rate')->first();

        $data['order']->offer = Offer::select('id', 'cost', 'time', 'driver_id')->where('status', 2)
                                        ->where('order_id', $id)
                                        ->with('Driver:id,name,image,average_rate')->first();

        unset($data['order']->user_id);
        unset($data['order']->created_at);
        unset($data['order']->updated_at);
        if ($data['order']->offer)
        {
            unset($data['order']->offer->driver_id);
            $data['order']->total = $data['order']->cost + $data['order']->vat;
            $data['order']->driver_cost = $data['order']->offer->cost;
            $data['order']->total_money = $data['order']->total + $data['order']->driver_cost;
        }
        else
        {
            $data['order']->total = $data['order']->cost + $data['order']->vat;
            $data['order']->driver_cost = null;
            $data['order']->total_money = $data['order']->total;
        }

        return $this->successResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->lang();
        $auth = $this->auth();
//        $rules =  [
//            'status'  => 'required',
//        ];
//
//        $validator = Validator::make(request()->all(), $rules);
//        $errors = $this->formatErrors($validator->errors());
//        if($validator->fails()) {return $this->errorResponse($errors);}

        Order::find($id)->update(request()->all());
        return $this->successResponse(null, __('api.OrderUpdated'));
    }

    public function offer($id)
    {
        $this->lang();
        $auth = $this->auth();
        $data['offers'] = Offer::where('order_id', $id)->select('id', 'time', 'cost', 'driver_id')
                                ->with('Driver:id,name,image,car_model,car_number,car_type')->get();

        $data['offers']->map(function ($item)
        {
            unset($item->driver_id);
        });

        return $this->successResponse($data);
    }

    public function driver($id)
    {
        $this->lang();
        $auth = $this->auth();
        $data['driver'] = Driver::where('id', $id)
                                ->select('id', 'name', 'phone', 'car_model', 'car_type', 'car_number', 'image', 'average_rate')
                                ->first();

        $data['driver']->rate_number = $data['driver']->Rate()->count();

        if ($data['driver']->rate_namber > 0)
        {
            $data['driver']->rates = $data['driver']->Rate()->where('type', 1)
                ->select('id', 'comment', 'rate', 'user_id', 'created_at as date')
                ->with("User:id,name,image")->get();
        }
        else
        {
            $data['driver']->rates= null;
        }

        return $this->successResponse($data);
    }

    public function addOffer($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'offer'  => 'required|exists:offers,id',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        Offer::find(request('offer'))->update(['status' => 2]);
        Order::find($id)->update(['status' => 2]);
        return $this->successResponse(null, __('api.OfferAccept'));
    }

    public function data($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $offer = Offer::where('order_id', $id)->where('status', 2)->first();
        $data['driver'] = $offer->Driver()->select('id', 'name', 'image')->first();

        $data['stores'] = Store::whereIn('id', OrderCart::where('order_id', $id)->select('store_id')->get())
                                ->select('id', "name_$lang as name", 'image')->get();

        return $this->successResponse($data);
    }

    public function rate($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'rate'  => 'required',
            'comment'  => 'required',
            'type'  => 'required',
            'id'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (request('type') == 2)
        {
            Rate::create([
                'rate' => request('rate'),
                'comment' => request('comment'),
                'order_id' => $id,
                'user_id' => $auth,
                'store_id' => request('id'),
            ]);

            $rates = Rate::where('store_id', request('id'))->get();
            Store::find(request('id'))->update(['average_rate' => ($rates->sum('rate') / count($rates))]);
        }
        else
        {
            Rate::create([
                'rate' => request('rate'),
                'comment' => request('comment'),
                'order_id' => $id,
                'user_id' => $auth,
                'type' => 1,
                'driver_id' => request('id'),
            ]);

            $rates = Rate::where('driver_id', request('id'))->where('type', 1)->get();
            Driver::find(request('id'))->update(['average_rate' => ($rates->sum('rate') / count($rates))]);
        }

        return $this->successResponse(null, __('api.RateSuccess'));
    }
}
