<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Cart;
use App\Product;
use App\Setting;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['cart'] = Cart::select('id', "quantity", 'product_id', 'price')
                        ->where('user_id', $auth)
                        ->where('status', 1)
                        ->get();

        $data['money'] = $data['cart']->sum('price');
        $data['cart']->map(function ($item) use ($lang)
        {
            $item->store = $item->Product->Store["name_$lang"];
            $item->name = $item->Product["name_$lang"];
            $item->image = $item->Product->image;
            $item->price = $item->Product->price;

            unset($item->product_id);
            unset($item->Product);
        });

        $data['vat'] = Setting::first()->vat;
        $data['total'] = Setting::first()->vat + $data['money'];

        return $this->successResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'product'  => 'required|exists:products,id',
            'quantity' => 'required|int'
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $item = Cart::where('product_id', request('product'))->where('user_id', $auth)->where('status', 1)->first();
        if ($item)
        {
            $item->update([
                'quantity' => $item->quantity + request('quantity'),
                'text' => request('text'),
                'price' => $item->price + ($item->Product->price * request('quantity')),
            ]);

            return $this->successResponse(null, __('api.IncreaseProduct'));
        }
        else
        {
            $product = Product::find(request('product'));
            Cart::create([
                'quantity' => request('quantity'),
                'text' => request('text'),
                'product_id' => request('product'),
                'user_id' => $auth,
                'price' => $product->price * request('quantity')
            ]);

            return $this->successResponse(null, __('api.Cart'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'cart'  => 'required|exists:carts,id',
            'quantity' => 'required|int'
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $item = Cart::find(request('cart'));
        $item->update([
            'quantity' => request('quantity'),
            'price' => $item->Product->price * request('quantity'),
        ]);

        return $this->successResponse(null, __('api.IncreaseProduct'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();
        return $this->successResponse(null, __('api.UnCart'));
    }
}
