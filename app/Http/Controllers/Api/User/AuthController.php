<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Auth;


class AuthController extends Controller
{


//    public function login(Request $request)
//    {
//        if (Auth::guard('apiUser')->attempt(['email'=>$request->email, 'password'=>$request->password])){
//            return redirect('admin');
//        }
//        flashy()->error(__('dashboard.login_fail'));
//        return back();
//    }

//    public function logout(Request $request)
//    {
//        Auth::guard('admin')->logout();
//
//        return redirect(route('admin.login'));
//    }


    public function login()
    {
        $this->lang();
        $rules =  [
            'email'  => 'required',
            'password'  => 'required',
        ];


        $validator = Validator::make(request()->all(), $rules);
        if($validator->fails()) {
            return $this->errorResponse($validator->errors()->all()[0]);
        }

        if (Auth::guard('apiUser')->attempt(['email' => request('email'), 'password' => request('password')]))
        {
            $auth = Auth::guard('apiUser')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['user'] = User::where('id', $auth->id)->select('id', 'name', 'image', 'phone', 'email', 'api_token')->first();
            return $this->successResponse($data);
        }
        if (Auth::guard('apiUser')->attempt(['phone' => request('email'), 'password' => request('password')]))
        {
            $auth = Auth::guard('apiUser')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['user'] = User::where('id', $auth->id)->select('id', 'name', 'image', 'phone', 'email', 'api_token')->first();
            return $this->successResponse($data);
        }
        return $this->errorResponse(__('api.LoginFail'),null);
    }

    public function register()
    {
        $this->lang();
        $rules =  [
            'name'  => 'required',
            'phone'  => 'required|unique:users',
            'email'  => 'required|unique:users',
            'password'  => 'required',
            'image'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('image');
        $input['api_token'] = str_random(70);

        if (request('image'))
        {
            $input['image'] = $this->uploadBase64(request('image'));
        }

        $auth = User::create($input);
        $data['user'] = User::where('id', $auth->id)->select('id', 'name', 'image', 'phone', 'email', 'api_token')->first();

        return $this->successResponse($data, __('api.RegisterSuccess'));
    }

    public function logout()
    {
        $this->lang();
        $auth = $this->auth();
        User::find($auth)->update(['api_token' => null]);
        return $this->successResponse(null, __('api.Logout'));
    }


}
