<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Rate;
use App\Store;
use App\StoreCategory;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $stores = Store::select('id', "name_$lang as name", 'image', 'rate as average_rate', 'min as minimum_order', 'type')
                        ->when(request('category'), function ($query)
                        {
                            $query->where('category_id',  request('category'));
                        })
                        ->when(request('region'), function ($query)
                        {
                            $query->where('region_id',  request('region'));
                        })
                        ->where('status', 2);

        switch (request('type'))
        {
            case 1 :
                $data['stores'] = $stores->orderBy('sort')->paginate(10);
                break;

            case 2 :
                $data['stores'] = $stores->orderBy('id', 'desc')->paginate(10);
                break;

            case 3 :
                $data['stores'] = $stores->orderBy('name')->paginate(10);
                break;

            case 4 :
                $data['stores'] = $stores->orderBy('min')->paginate(10);
                break;

            case 5 :
                $data['stores'] = $stores->orderBy('sort')->paginate(10);
                break;

            default :
                $data['stores'] = $stores->orderBy('sort')->paginate(10);
                break;
        }

        $data['stores']->map(function ($item) use ($auth)
        {
            $item->is_like = $item->IsLike($auth);
            $item->is_open = $item->type == 1 ? false : true;
            $item->rate_number = $item->Rate()->count();
            $item->images = $item->Image()->pluck('image');

            unset($item->type);
        });
        return $this->successResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['categories'] = StoreCategory::select('id', "name_$lang as name")
                            ->where('store_id', $id)
                            ->get();

        $data['categories']->map(function ($item) use ($auth, $lang)
        {
            $item->products = $item->Product()->select('id', "name_$lang as name", "desc_$lang as description", 'image', 'price')
                                    ->orderBy('id', 'desc')->get();

            $item->products->map(function ($product) use ($auth)
            {
                $product->is_like = $product->IsLike($auth);
                $product->images = $product->Image()->pluck('image');
            });

            unset($item->category_id);
        });

        $data['categories']->filter(function ($item)
        {
            return count($item->products) > 0;
        });

        return $this->successResponse($data);
    }   
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rate($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['rates'] = Rate::select('id', 'user_id', 'comment', 'rate', 'created_at as date')
            ->where('store_id', $id)
            ->get();

        $data['rates']->map(function ($item)
        {
            $item->name = $item->User->name;
            $item->image = $item->User->image;

            unset($item->user_id);
            unset($item->User);
        });

        return $this->successResponse($data);
    }
}
