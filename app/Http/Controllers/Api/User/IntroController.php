<?php

namespace App\Http\Controllers\Api\User;


use App\Admin;
use App\Banner;
use App\Category;
use App\Http\Controllers\Controller;
use App\Region;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class IntroController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $lang = request()->header('lang');
        $data['intro']['banners'] = Banner::select("image")->get()->pluck("image");
        $data['intro']['categories'] = Category::select("id", "name_$lang as name", "image")->get();
        $data['intro']['regions'] = Region::select("id", "name_$lang as name")->get();

        return $this->successResponse($data);
    }

}
