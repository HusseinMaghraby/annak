<?php

namespace App\Http\Controllers\Api\User;


use App\Admin;
use App\Banner;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Region;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class BannerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = request()->header('lang');
        $data['region'] = Banner::select("image")->get()->pluck("image");
        return $this->successResponse($data);
    }

}