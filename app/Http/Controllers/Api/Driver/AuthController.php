<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Driver;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function login()
    {
        $lang = $this->lang();
        $rules =  [
            'email'  => 'required',
            'password'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (Auth::guard('apiDriver')->attempt(['email' => request('email'), 'password' => request('password')]) &&
            Driver::where('email', request('email'))->where('status', 2)->first())
        {
            $auth = Auth::guard('apiDriver')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['driver'] = Driver::select('id', "name", 'image', 'rate as average_rate', 'type', 'city', 'bank', 'account',
                                            'email', 'phone', 'api_token', 'car_licence', 'id_image', 'personal_licence',
                                            'car_type', 'car_model', 'car_number')
                                            ->where('id', $auth->id)->first();

            $data['driver']->is_open = $data['driver']->type == 1 ? false : true;
            unset($data['driver']->type);

            return $this->successResponse($data);
        }
        if (Auth::guard('apiDriver')->attempt(['phone' => request('email'), 'password' => request('password')]) &&
            Driver::where('phone', request('email'))->where('status', 2)->first())
        {
            $auth = Auth::guard('apiDriver')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['driver'] = Driver::select('id', "name", 'image', 'rate as average_rate', 'type', 'city', 'bank', 'account',
                                                'email', 'phone', 'api_token', 'car_licence', 'id_image', 'personal_licence',
                                                'car_type', 'car_model', 'car_number')
                                                ->where('id', $auth->id)->first();

            $data['driver']->is_open = $data['driver']->type == 1 ? false : true;
            unset($data['driver']->type);

            return $this->successResponse($data);
        }
        return $this->errorResponse(__('api.LoginFail'));
    }

    public function register()
    {
        $this->lang();
        $rules =  [
            'name'  => 'required',
            'account'  => 'required',
            'bank'  => 'required',
            'city'  => 'required',
            'phone'  => 'required|unique:drivers',
            'email'  => 'required|unique:drivers',
            'password'  => 'required',
            'car_type'  => 'required',
            'car_model'  => 'required',
            'car_number'  => 'required',
            'car_licence'  => 'required',
            'personal_licence'  => 'required',
            'id_image'  => 'required',
            'image'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('image', 'car_licence', 'personal_licence', 'city', 'id_licence');
        $input['api_token'] = str_random(70);
        $input['city_id'] = request()->city;

        $item = Driver::create($input);

        if (request('image'))
        {
            $item->image = $this->uploadBase64(request('image'), 'drivers/'.$item->id);
            $item->save();
        }

        if (request('id_image'))
        {
            $item->image = $this->uploadBase64(request('id_image'), 'drivers/'.$item->id);
            $item->save();
        }

        if (request('car_licence'))
        {
            $item->image = $this->uploadBase64(request('car_licence'), 'drivers/'.$item->id);
            $item->save();
        }

        if (request('personal_licence'))
        {
            $item->image = $this->uploadBase64(request('personal_licence'), 'drivers/'.$item->id);
            $item->save();
        }

        return $this->successResponse(null, __('api.WaitingActivate'));
    }

    public function logout()
    {
        $this->lang();
        $auth = $this->auth();
        Driver::find($auth)->update(['api_token' => null]);
        return $this->successResponse(null, __('api.Logout'));
    }
}
