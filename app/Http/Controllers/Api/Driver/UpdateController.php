<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Driver;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{

    public function index()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
            'is_open'  => 'required',
            'account'  => 'required',
            'bank'  => 'required',
            'city'  => 'required',
            'phone'  => 'required|unique:drivers',
            'email'  => 'required|unique:drivers',
            'password'  => 'required',
            'car_type'  => 'required',
            'car_model'  => 'required',
            'car_number'  => 'required',
            'car_licence'  => 'nullable',
            'personal_licence'  => 'nullable',
            'id_image'  => 'nullable',
            'image'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('image', 'car_licence', 'personal_licence', 'id_licence');
        $item = Driver::find($auth);

        if (request('image'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->image);
                Storage::disk('public')->delete($image);
            }
            $input['image'] = $this->uploadBase64(request('image'), 'drivers/'.$auth);
        }

        if (request('id_image'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->id_image);
                Storage::disk('public')->delete($image);
            }
            $input['id_image'] = $this->uploadBase64(request('id_image'), 'drivers/'.$auth);
        }

        if (request('car_licence'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->car_licence);
                Storage::disk('public')->delete($image);
            }
            $input['car_licence'] = $this->uploadBase64(request('car_licence'), 'drivers/'.$auth);
        }

        if (request('personal_licence'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->personal_licence);
                Storage::disk('public')->delete($image);
            }
            $input['personal_licence'] = $this->uploadBase64(request('personal_licence'), 'drivers/'.$auth);
        }
        $item->updated($input);

        return $this->successResponse(null, __('api.ProfileUpdated'));
    }
}
