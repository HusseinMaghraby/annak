<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Cart;
use App\Driver;
use App\Offer;
use App\Order;
use App\OrderCart;
use App\Rate;
use App\Setting;
use App\Store;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['orders'] = Order::whereIn('status', [1 , 2, 3, 4, 5, 6, 7, 8])
                                ->whereIn('id', OrderCart::where('store_id', $auth)->select('order_id')->get())->get();

        $data['orders']->map(function ($item) use ($auth)
        {
            $item->status = (int)$item->status;
            if ($item->payment) $item->payment = (int)$item->payment;
            $item->type = (int)$item->type;
            $item->date = date('Y-m-d h:i A', strtotime($item->created_at));
            $item->orders_number = $item->Cart()->where('store_id', $auth)->count();

            $item->user = $item->User()->select('id', 'name', 'image', 'average_rate')->first();

            $driver = Offer::where('status', 2)->where('order_id', $item->id)->first();
            if ($driver)
            {
                $item->driver = Driver::where('id', $driver->driver_id)->select('id', 'name', 'image')->first();
            }
            else
            {
                $item->driver = null;
            }

            unset($item->user_id);
            unset($item->User);
            unset($item->Cart);
            unset($item->created_at);
            unset($item->updated_at);
            if ($item->offer) unset($item->offer->driver_id);
        });

        return $this->successResponse($data);
    }

    public function previous()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['orders'] = Order::whereIn('status', [9, 10])
                                ->whereIn('id', OrderCart::where('store_id', $auth)->select('order_id')->get())->get();

        $data['orders']->map(function ($item) use ($auth)
        {
            $item->status = (int)$item->status;
            if ($item->payment) $item->payment = (int)$item->payment;
            $item->type = (int)$item->type;
            $item->date = date('Y-m-d h:i A', strtotime($item->created_at));
            $item->orders_number = $item->Cart()->where('store_id', $auth)->count();

            $item->user = $item->User()->select('id', 'name', 'image', 'average_rate')->first();

            $driver = Offer::where('status', 2)->where('order_id', $item->id)->first();
            $item->driver = Driver::where('id', $driver->driver_id)->select('id', 'name', 'image')->first();

            unset($item->user_id);
            unset($item->User);
            unset($item->Cart);
            unset($item->created_at);
            unset($item->updated_at);
            if ($item->offer) unset($item->offer->driver_id);
        });

        return $this->successResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['order'] = Order::find($id);

        $data['order']->status = (int)$data['order']->status;
        if ($data['order']->payment)$data['order']->payment = (int)$data['order']->payment;
        $data['order']->type = (int)$data['order']->type;
        $data['order']->date = date('Y-m-d h:i A', strtotime($data['order']->created_at));

        $data['order']->user = $data['order']->User()->select('id', 'name', 'image', 'average_rate')->first();

        $data['order']->products = Cart::whereIn('id', $data['order']->Cart()->where('store_id', $auth)->select('cart_id')->get())
                                        ->select('id', 'quantity', 'price', 'product_id')->get();

        $data['order']->products->map(function ($item) use ($lang)
        {
            $item->name = $item->Product["name_$lang"];
            $item->image = $item->Product->image;

            unset($item->product_id);
            unset($item->Product);
        });

        $data['order']->rate = Rate::where('order_id', $id)->where('store_id', $auth)
                                        ->select('id', 'comment', 'rate')->first();

        $driver = Offer::where('status', 2)->where('order_id', $id)->first();
        if ($driver)
        {
            $data['order']->driver = Driver::where('id', $driver->driver_id)->select('id', 'name', 'image', 'average_rate')->first();
        }
        else
        {
            $data['driver'] = null;
        }

        unset($data['order']->User);
        unset($data['order']->user_id);
        unset($data['order']->created_at);
        unset($data['order']->updated_at);

        return $this->successResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'status'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        Order::find($id)->update(request()->all());
        return $this->successResponse(null, __('api.OrderUpdated'));
    }
}
