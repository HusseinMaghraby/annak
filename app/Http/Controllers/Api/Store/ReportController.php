<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Cart;
use App\Order;
use App\OrderCart;
use App\Product;
use App\StoreCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $carts = OrderCart::where('store_id', $auth)->where('created_at', '=', date('Y-m'));

        $product = Cart::whereIn('id', $carts->select('cart_id')->get())
                        ->select('product_id', DB::raw('SUM(quantity) as quantity'))
                        ->groupBy('product_id')
                        ->orderBy('quantity', 'desc')
                        ->first();

        $data['most_sell'] = $product ? $product->Product["name$lang"] : null;

        $data['total'] = Cart::whereIn('id', $carts->select('cart_id')->get())->get()->sum('price');

        $data['orders'] = $carts->select('order_id')->groupBy('order_id')->count();

        $data['cash'] = Order::whereIn('id', $carts->select('order_id')->groupBy('order_id')->get())
                                ->where('payment', 1)->get()->sum('cost');

        $data['visa'] = Order::whereIn('id', $carts->select('order_id')->groupBy('order_id')->get())
                                ->where('payment', 2)->get()->sum('cost');

        return $this->successResponse($data);
    }

    public function custom()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $rules =  [
            'from'  => 'required',
            'to'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (request('category'))
        {
            $products = Product::whereIn('storeCategory_id',
                                            StoreCategory::where('store_id', $auth)->select('id')->get())
                                ->select('id')->get();
        }
        $carts = OrderCart::where('store_id', $auth)
                    ->when($products, function ($query) use ($products)
                    {
                        $query->whereIn('product_id', $products);
                    })
                    ->whereBetween('created_at',
                    [date('Y-m-d', strtotime(request('from'))), date('Y-m-d', strtotime(request('to')))]);

        $data['products'] = Cart::whereIn('id', $carts->select('cart_id')->get())
                        ->select('product_id', DB::raw('SUM(quantity) as quantity'), DB::raw('SUM(price) as cost'))
                        ->groupBy('product_id')
                        ->get();

        $data['products']->map(function ($item) use ($lang)
        {
            $item->name = $item->Product["name_$lang"];

            unset($item->Product);
        });

        $order = Order::whereIn('id', $carts->select('order_id')->groupBy('order_id')->get())->where('payment', 1)->get();
        $data['cash']->quantity = count($order);
        $data['cash']->cost = $order->sum('cost');

        $order = Order::whereIn('id', $carts->select('order_id')->groupBy('order_id')->get())->where('payment', 2)->get();
        $data['visa']->quantity = count($order);
        $data['visa']->cost = $order->sum('cost');

        return $this->successResponse($data);
    }
}
