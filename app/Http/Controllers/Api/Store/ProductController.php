<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['products'] = Product::select('id', "name_$lang as name", 'image', 'price', "desc_$lang as description", 'storeCategory_id')
                        ->where('store_id', $auth)
                        ->orderBy('id', 'desc')
                        ->paginate(10);

        $data['products']->map(function ($item) use ($auth, $lang)
        {
            $item->category = $item->Category()->select('id', "name_$lang as name")->first();
            $item->images = $item->Image()->pluck('image');

            unset($item->category_id);
            unset($item->Category);
        });
        return $this->successResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
            'description'  => 'required',
            'price'  => 'required',
            'category'  => 'required|exists:store_categories,id',
            'image'  => 'required',
            'images'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('name', 'description', 'category', 'image', 'images');
        $input['store_id'] = $auth;
        $input['name_ar'] = request('name');
        $input['name_en'] = request('name');
        $input['desc_ar'] = request('description');
        $input['desc_en'] = request('description');
        $input['storeCategory_id'] = request('category');

        $item = Product::create($input);

        if (request('image'))
        {
            $item->image = $this->uploadBase64(request('image'), 'stores/'.$auth.'/'.$item->id);
            $item->save();
        }

        if (request('images'))
        {
            foreach (request('images') as $image)
            {
                ProductImage::create([
                    'image' => $this->uploadBase64($image, 'stores/'.$item->id),
                    'product_id' => $item->id
                ]);
            }
        }
        return $this->successResponse(null, __('api.Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['product'] = Product::select("name_$lang as name", 'id', "desc_$lang as description", 'price', 'storeCategory_id', 'image')
                        ->where('id', $id)
                        ->first();

        $data['product']->images = $data['product']->Image()->pluck('image');
        $data['product']->category = $data['product']->Category()->select('id', "name_$lang as name")->first();

        unset($data['product']->Category);
        unset($data['product']->storeCategory_id);

        return $this->successResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
            'description'  => 'required',
            'price'  => 'required',
            'category'  => 'required|exists:store_categories,id',
            'image'  => 'nullable',
            'images'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('name', 'description', 'category', 'image', 'images');
        $input['name_ar'] = request('name');
        $input['name_en'] = request('name');
        $input['desc_ar'] = request('description');
        $input['desc_en'] = request('description');
        $input['storeCategory_id'] = request('category');

        $item = Product::find($id);

        if (request('image'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->image);
                Storage::disk('public')->delete($image);
            }
            $input['image'] = $this->uploadBase64(request('image'), 'stores/'.$auth.'/'.$id);
        }


        foreach ($item->Image()->get() as $data)
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $data->image);
                Storage::disk('public')->delete($image);
            }
            $data->delete();
        }

        foreach (request('images') as $image)
        {
            ProductImage::create([
                'image' => $this->uploadBase64($image, 'stores/'.$item->id),
                'product_id' => $item->id
            ]);
        }

        $item->update($input);

        return $this->successResponse(null, __('api.Updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Product::find($id);
        Storage::deleteDirectory('uploads/stores/'.$item->store_id.'/'.$id);
        $item->delete();
        return $this->successResponse(null, __('api.Deleted'));
    }
}
