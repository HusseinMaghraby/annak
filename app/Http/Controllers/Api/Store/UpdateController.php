<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Category;
use App\Store;
use App\StoreCategory;
use App\StoreImage;
use App\StoreLicence;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{

    public function index()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
            'is_open'  => 'required',
            'account'  => 'required',
            'bank'  => 'required',
            'city'  => 'required',
            'region'  => 'required|exists:regions,id',
            'category'  => 'required|exists:categories,id',
            'phone'  => 'required|unique:stores,phone,'.$auth,
            'address'  => 'required',
            'longitude'  => 'required',
            'latitude'  => 'required',
            'email'  => 'required|unique:stores,email,'.$auth,
            'licence'  => 'required',
            'image'  => 'nullable',
            'images'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('image', 'region','images', 'licence', 'address', 'name', 'category');
        $input['name_ar'] = request('name');
        $input['name_en'] = request('name');
        $input['address_ar'] = request('address');
        $input['address_en'] = request('address');
        $input['region_id'] = request('region');
        $input['category_id'] = request('category');

        $item = Store::find($auth);

        if (request('image'))
        {
            if (strpos($item->image, '/uploads/') !== false) {
                $image = str_replace( asset('').'storage/', '', $item->image);
                Storage::disk('public')->delete($image);
            }
            $input['image'] = $this->uploadBase64(request('image'), 'stores/'.$auth);
        }

        if (request('licence'))
        {
            foreach ($item->Licence()->get() as $licence) {
                Storage::delete($licence->image);
                $licence->delete();
            }

            foreach (request('licence') as $licence) {
                StoreLicence::create([
                    'image' => $this->uploadBase64($licence, 'stores/'.$auth),
                    'store_id' => $item->id
                ]);
            }
        }

        if (request('images'))
        {
            foreach ($item->Image()->get() as $image) {
                Storage::delete($image->image);
                $image->delete();
            }

            foreach (request('images') as $image) {
                StoreImage::create([
                    'image' => $this->uploadBase64($image, 'stores/'.$auth),
                    'store_id' => $item->id
                ]);
            }
        }
        $item->updated($input);

        return $this->successResponse(null, __('api.ProfileUpdated'));
    }
}
