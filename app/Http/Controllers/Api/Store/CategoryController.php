<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\StoreCategory;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['categories'] = StoreCategory::where('store_id', $auth)->select('id', "name_$lang as name")->get();

        return $this->successResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        StoreCategory::create([
            'name_ar' => request('name'),
            'name_en' => request('name'),
            'store_id' => $auth
        ]);
        return $this->successResponse(null, __('api.Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lang = $this->lang();
        $auth = $this->auth();
        $data['category'] = StoreCategory::select('id', "name_$lang as name")
                                        ->where('id', $id)
                                        ->first();

        return $this->successResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->lang();
        $auth = $this->auth();
        $rules =  [
            'name'  => 'required',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        StoreCategory::find($id)->update(['name_ar' => request('name')]);
        return $this->successResponse(null, __('api.Updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StoreCategory::find($id)->delete();
        return $this->successResponse(null, __('api.Deleted'));
    }
}
