<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Image;
use App\License;
use App\Store;
use App\StoreImage;
use App\StoreLicence;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Auth;


class AuthController extends Controller
{


//    public function login(Request $request)
//    {
//        if (Auth::guard('apiUser')->attempt(['email'=>$request->email, 'password'=>$request->password])){
//            return redirect('admin');
//        }
//        flashy()->error(__('dashboard.login_fail'));
//        return back();
//    }
//
//    public function logout(Request $request)
//    {
//        Auth::guard('admin')->logout();
//
//        return redirect(route('admin.login'));
//    }


    public function login()
    {
        $this->lang();
        $rules =  [
            'email'  => 'required',
            'password'  => 'required',
        ];


        $validator = Validator::make(request()->all(), $rules);
        if($validator->fails()) {
            return $this->errorResponse($validator->errors()->all()[0]);
        }

        if (Auth::guard('apiStore')->attempt(['email' => request('email'), 'password' => request('password')]))
        {

            $auth = Auth::guard('apiStore')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['store'] = Store::where('id', $auth->id)->select('id','name_ar', 'name_en', 'image', 'phone', 'email', 'api_token')->first();
            return $this->successResponse($data);
        }
        if (Auth::guard('apiStore')->attempt(['phone' => request('email'), 'password' => request('password')]))
        {
            $auth = Auth::guard('apiStore')->user();
            $auth->update(['api_token' => str_random(70)]);

            $data['store'] = Store::where('id', $auth->id)->select('id', 'name_ar', 'name_en', 'image', 'phone', 'email', 'api_token')->first();
            return $this->successResponse($data);
        }
        return $this->errorResponse( __('api.LoginFail'),null);
    }

    public function register()
    {
        $this->lang();
        $rules =  [
            'name'  => 'required',
            'phone'  => 'required|unique:stores',
            'email'  => 'required|unique:stores',
            'account'  => 'required',
            'bank'  => 'required',
            'city'  => 'required',
            'region'  => 'required',
            'category'  => 'required',
            'address'  => 'required',
            'longitude'  => 'required',
            'latitude'  => 'required',
            'password'  => 'required',
            'image'  => 'required',
            'licenses'  => 'required',
            'images'  => 'nullable',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $input = request()->except('name', 'address', 'image', 'images', 'licenses', 'city', 'region', 'category');
        $input['api_token'] = str_random(70);
        $input['name_ar'] = request()->name;
        $input['name_en'] = request()->name;
        $input['address_ar'] = request()->address;
        $input['address_en'] = request()->address;
        $input['city_id'] = request()->city;
        $input['region_id'] = request()->region;
        $input['category_id'] = request()->category;

        if (request('image'))
        {
            $input['image'] = $this->uploadBase64(request('image'));
        }

        $auth = Store::create($input);

        if( request ('images')){

            StoreImage::create([
                'image' => $this->uploadBase64(request('images')),
                'store_id' => $auth->id,
            ]);
        }

        if (request ('licenses')) {
            StoreLicence::create([
                'image' => $this->uploadBase64(request ('licenses')),
                'store_id' => $auth->id,
            ]);
        }

        $lang = request()->header('lang');
        $data['store'] = Store::where('id', $auth->id)->select('id',
            "name_$lang as name",
            'category_id',
            'city_id',
            'region_id',
            'email',
            'image',
            'phone',
            "address_$lang as address",
            'bank',
            'account',
            'image',
            'api_token')->first();

        $data['store']->category_name = $data['store']->category["name_$lang"];
        $data['store']->city_name = $data['store']->city["name_$lang"];
        $data['store']->region_name = $data['store']->region["name_$lang"];

        unset($data['store']->category);
        unset($data['store']->city);
        unset($data['store']->region);
        unset($data['store']->category_id);
        unset($data['store']->city_id);
        unset($data['store']->region_id);

        return $this->successResponse($data, __('api.RegisterSuccess'));
    }
}
