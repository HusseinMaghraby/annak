<?php

namespace App\Http\Controllers\Api;


use App\Admin;
use App\Category;
use App\Http\Controllers\Controller;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;

class SettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = request()->header('lang');
        $data['setting'] = Setting::select(
            "facebook",
            "instagram",
            "email",
            "twitter",
            "whatsapp",
            "phone",
            "about_$lang as about",
            "privacy_$lang as privacy",
            'banner'
        )->get();
        return $this->successResponse($data);
    }

}
