<?php

namespace App\Http\Controllers\Api;

use App\Driver;
use App\Http\Controllers\Controller;
use App\Category;
use App\Store;
use App\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function social()
    {
        $lang = $this->lang();
        $rules =  [
            'email'  => 'required',
            'type'  => 'required|in:1,2,3',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (request('type') == 1)
        {
            $auth = User::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['api_token' => str_random(70)]);
                $data = User::where('id', $auth->id)->select('id', 'name', 'image', 'phone', 'email')->first();

                return $this->successResponse($data);
            }
        }
        elseif (request('type') == 2)
        {
            $auth = Store::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['api_token' => str_random(70)]);

                $data = Store::select('id',
                    "name_$lang as name",
                    'email',
                    'image',
                    'phone',
                    "address_$lang as address",
                    'bank',
                    'account',
                    'image')->where('id', $auth->id)->first();





                return $this->successResponse($data);
            }
        }

        else
        {
            $auth = Driver::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['api_token' => str_random(70)]);
                $data = Driver::where('id', $auth->id)->first();

                return $this->successResponse($data);
            }
        }
        return $this->errorResponse(__('api.NotFount'));
    }

    public function email()
    {
        $lang = $this->lang();
        $rules =  [
            'email'  => 'required',
            'type'  => 'in:1,2,3',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $code = rand(1000, 9999);

        if (request('type')) {
            if (request('type') == 1) {
                $auth = User::where('email', request('email'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                }
                else
                {
                    return $this->errorResponse(__('api.EmailNotFount'));
                }
            } elseif (request('type') == 2) {
                $auth = Store::where('email', request('email'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                }
                else
                {
                    return $this->errorResponse(__('api.EmailNotFount'));
                }
            }
                else {
                $auth = Driver::where('email', request('email'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                }
                else
                {
                    return $this->errorResponse(__('api.EmailNotFount'));
                }
            }
        }

        $data['code'] = $code;
        return $this->successResponse($data);
    }

    public function phone()
    {
        $lang = $this->lang();
        $rules =  [
            'phone'  => 'required',
            'type'  => 'in:1,2,3',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        $code = rand(1000, 9999);

        if (request('type')) {
            if (request('type') == 1) {
                $auth = User::where('phone', request('phone'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                } else {
                    return $this->errorResponse(__('api.PhoneNotFount'));
                }
            } elseif (request('type') == 2) {
                $auth = Store::where('phone', request('phone'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                } else {
                    return $this->errorResponse(__('api.PhoneNotFount'));
                }
            }
            else {
                $auth = Driver::where('phone', request('phone'))->first();
                if ($auth) {
                    $data['code'] = $code;
                    return $this->successResponse($data);
                } else {
                    return $this->errorResponse(__('api.PhoneNotFount'));
                }
            }
        }

        $data['code'] = $code;
        return $this->successResponse($data);
    }

    public function resetPassword()
    {
        $lang = $this->lang();
        $rules =  [
            'email'  => 'required',
            'password'  => 'required',
            'type'  => 'required|in:1,2,3',
        ];

        $validator = Validator::make(request()->all(), $rules);
        $errors = $this->formatErrors($validator->errors());
        if($validator->fails()) {return $this->errorResponse($errors);}

        if (request('type') == 1)
        {
            $auth = User::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }

            $auth = User::where('phone', request('phone'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }
        }
        elseif (request('type') == 2)
        {
            $auth = Store::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }

            $auth = Store::where('phone', request('phone'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }
        }
        else
        {
            $auth = Driver::where('email', request('email'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }

            $auth = Driver::where('phone', request('phone'))->first();
            if ($auth)
            {
                $auth->update(['password' => request('password')]);
                return $this->successResponse(null, __('api.PasswordReset'));
            }
        }
        return $this->errorResponse(__('api.NotFount'));
    }
}
