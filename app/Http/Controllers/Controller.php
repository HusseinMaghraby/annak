<?php

namespace App\Http\Controllers;

use App\Store;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\File\File;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    # --------------------successResponse------------------
    public function successResponse($data, $message = NULL)
    {
        $response = array(
            'status'  => TRUE,
            'message' => $message,
            'data'    => $data
        );
        return response()->json($response, 200);
    }


    # --------------------errorResponse------------------
    public function errorResponse($errors , $data = NULL)
    {
        $response = array(
            'status'  => FALSE,
            'message' => $errors,
            'data'    => $data
        );
        return response()->json($response);
    }

    #------------------ format error----------------
    public function formatErrors($errors)
    {
        $stringError = "";
        for ($i=0; $i < count($errors->all()); $i++) {
            $stringError = $stringError . $errors->all()[$i];
            if($i != count($errors->all())-1){
                $stringError = $stringError . '\n';
            }
        }
        return $stringError;
    }

    #------------------ lang ----------------
    public function lang()
    {
        App::setLocale(request()->header('lang'));
        if (request()->header('lang'))
        {
            return request()->header('lang');
        }
        return 'ar';
    }

    #------------------ Auth ----------------
    public function auth()
    {
        if (request()->header('Authorization'))
        {
            $user = User::where('api_token', request()->header('Authorization'))->first();
            $store = Store::where('api_token', request()->header('Authorization'))->first();
//            $driver = Driver::where('api_token', request()->header('Authorization'))->first();

            if ($user)
            {
                return $user->id;
            }
            if ($store)
            {
                return $store->id;
            }
//            if ($driver)
//            {
//                return $driver->id;
//            }
        }
        return 0;
    }

    # -------------------------------------------------
    public function uploadBase64($base64, $extension='jpeg')
    {
//        $base64 = preg_replace('/data:image\/(.*?);base64,/','',$base64);
//        $base64 = str_replace(' ', '+', $base64);
//        $fileBaseContent = base64_decode($base64);
//        $fileName = str_random(10).'_'.time().'.'.$extension;
//        $fileBaseContent->move('images', $fileName);
//         return $fileName;

        $base64 = preg_replace('/data:image\/(.*?);base64,/','',$base64);
        $base64 = str_replace(' ', '+', $base64);
        $tmpFilePath=sys_get_temp_dir().'/'.uniqid();
        file_put_contents($tmpFilePath, $base64);
        $fileName = str_random(10).'_'.time().'.'.$extension;
        $tmpFile=new File($tmpFilePath);
        $tmpFile->move(public_path('images'), $fileName);
        return $fileName;

    }

}