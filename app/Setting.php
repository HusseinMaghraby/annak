<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $guarded = [];

    public function getImageAttribute($image){

        return asset('images/'.$image);
    }
}
