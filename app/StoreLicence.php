<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreLicence extends Model
{
    //
    protected $guarded = [];

    public function getImageAttribute($image)
    {

        return asset('images/' . $image);
    }

    public function store(){
        return $this->belongsTo('App\Store', 'store_id');
    }

}
