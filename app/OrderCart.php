<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCart extends Model
{
    //
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function cart(){
        return $this->belongsTo('App\Cart', 'cart_id');
    }

    public function store(){
        return $this->belongsTo('App\Store', 'store_id');
    }

    public function order(){
        return $this->belongsTo('App\Order', 'order_id');
    }
}
