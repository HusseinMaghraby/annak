<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('apiLocale')->namespace('Api\Store')->group(function ()
{
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');

    Route::middleware('api:apiStore')->group(function ()
    {
        Route::post('profile-update', 'UpdateController@index');
        Route::post('custom-report', 'ReportController@custom');
        Route::get('orders/previous', 'OrderController@previous');
        Route::get('logout', 'AuthController@logout');

        Route::resource('categories', 'CategoryController');
        Route::resource('orders', 'OrderController');
        Route::resource('products', 'ProductController');
        Route::resource('reports', 'ReportController');
    });
});
