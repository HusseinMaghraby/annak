<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('apiLocale')->namespace('Api')->group(function () {

    Route::post('social-login', 'AuthController@social');
    Route::post('email-verification', 'AuthController@email');
    Route::post('phone-verification', 'AuthController@phone');
    Route::post('reset-password', 'AuthController@resetPassword');


    Route::resource('user/intro', 'IntroController');
    Route::resource('user/intro', 'IntroController');
    Route::resource('user/banners', 'BannerController');
    Route::resource('settings', 'SettingController');
    Route::resource('categories', 'CategoryController');
    Route::resource('cites', 'CityController');
    Route::resource('regions', 'RegionController');
    Route::resource('stores', 'StoreController');
    Route::resource('suggestions', 'SuggestionController');

});