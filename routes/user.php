<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('apiLocale')->namespace('Api\User')->group(function ()
{
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');

    Route::get('stores/{id}/rates', 'StoreController@rate');

    Route::resource('banners', 'BannerController');
    Route::resource('intro', 'IntroController');
    Route::resource('stores', 'StoreController');

    Route::middleware('api:apiUser')->group(function ()
    {
        Route::get('logout', 'AuthController@logout');
        Route::get('orders/{id}/offers', 'OrderController@offer');
        Route::get('drivers/{id}', 'OrderController@driver');
        Route::get('orders/{id}/data', 'OrderController@data');
        Route::post('orders/{id}/rate', 'OrderController@rate');
        Route::post('orders/{id}/offers', 'OrderController@addOffer');

        Route::resource('orders', 'OrderController');
        Route::resource('cart', 'CartController');
        Route::resource('favorites', 'FavoriteController');
        Route::resource('orders', 'OrderController');
    });
});
