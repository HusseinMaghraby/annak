<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return redirect('/ar/dashboard');
});

Route::get('admin/signin', 'Dashboard\LoginController@index')->name('admin.login');
Route::post('admin/signin', 'Dashboard\LoginController@login')->name('admin.login');

Route::prefix('{lang}/dashboard')->namespace('Dashboard')->name('admin.')->middleware(['admin:admin', 'locale'])->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::delete('admins/multiDelete', 'AdminController@multiDelete')->name('admins.multiDelete');
    Route::any('admins/search', 'AdminController@search')->name('admins.search');
    Route::resource('admins', 'AdminController');

    Route::delete('banners/multiDelete', 'BannerController@multiDelete')->name('banners.multiDelete');
    Route::resource('banners', 'BannerController');

    Route::delete('users/multiDelete', 'UserController@multiDelete')->name('users.multiDelete');
    Route::any('users/search', 'UserController@search')->name('users.search');
    Route::resource('users', 'UserController');

    Route::delete('categories/multiDelete', 'CategoryController@multiDelete')->name('categories.multiDelete');
    Route::any('categories/search', 'CategoryController@search')->name('categories.search');
    Route::resource('categories', 'CategoryController');

    Route::delete('cities/multiDelete', 'CityController@multiDelete')->name('cities.multiDelete');
    Route::any('cities/search', 'CityController@search')->name('cities.search');
    Route::resource('cities', 'CityController');

    Route::delete('rates/multiDelete', 'RateController@multiDelete')->name('rates.multiDelete');
    Route::resource('rates', 'RateController');

    Route::delete('regions/multiDelete', 'RegionController@multiDelete')->name('regions.multiDelete');
    Route::any('regions/search', 'RegionController@search')->name('regions.search');
    Route::resource('regions', 'RegionController');

    Route::delete('stores/multiDelete', 'StoreController@multiDelete')->name('stores.multiDelete');
    Route::any('stores/search', 'StoreController@search')->name('stores.search');
    Route::resource('stores', 'StoreController');

    Route::delete('images/multiDelete', 'ImageController@multiDelete')->name('images.multiDelete');
    Route::get('images/{id}/create', 'ImageController@create')->name('images.creates');
    Route::post('images/{id}/store', 'ImageController@store')->name('images.stores');
    Route::resource('images', 'ImageController');

    Route::delete('licenses/multiDelete', 'LicenseController@multiDelete')->name('licenses.multiDelete');
    Route::get('licenses/{id}/create', 'LicenseController@create')->name('licenses.creates');
    Route::post('licenses/{id}/store', 'LicenseController@store')->name('licenses.stores');
    Route::resource('licenses', 'LicenseController');

    Route::delete('suggestions/multiDelete', 'SuggestionController@multiDelete')->name('suggestions.multiDelete');
    Route::any('suggestions/search', 'SuggestionController@search')->name('suggestions.search');
    Route::resource('suggestions', 'SuggestionController');

    Route::resource('settings', 'SettingController');
});
