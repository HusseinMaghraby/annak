<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('apiLocale')->namespace('Api\Driver')->group(function ()
{
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');

    Route::middleware('api:apiDriver')->group(function ()
    {
        Route::get('logout', 'AuthController@logout');
        Route::post('profile-update', 'UpdateController@index');
    });
});
