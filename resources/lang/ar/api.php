<?php
return [
    "LoginFail" =>'خطأ في التسجيل',
    "RegisterSuccess" =>'تم التسجيل بنجاح',
    "PasswordReset" =>'تم تغيير كلمة المرور بنجاح',
];