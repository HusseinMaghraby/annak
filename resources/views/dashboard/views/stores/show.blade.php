@php
  $headers = [
              $resource['header'] => $resource['route'].'.index',
              $resource['action'] => '#',
          ];
@endphp
@extends('dashboard.layouts.app')
@section('title', __('dashboard.'.$resource['title']))
@section('content')
  @include('dashboard.components.header')
<div class="content">
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-fw fa-{{$resource['icon']}}"> </i> {{__('dashboard.'.$resource['header'])}}</h3>
  </div>

  <table class="table table-hover">
    <tr>
      <td><strong>{{ __('dashboard.Name') }}</strong></td>
      <td>{{ $item['name_'.App::getLocale()] }}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Email') }}</strong></td>
      <td>{{ $item->email }}</td>
    </tr>
    <tr>
      <td><strong>{{__('dashboard.Phone') }}</strong></td>
      <td>{{ $item->phone }}</td>
    </tr>
    <tr>
      <td><strong>{{__('dashboard.Image') }}</strong></td>
      <td>
        @if($item->image == NULL)
          <i class="fa fa-fw fa-image"> </i>
        @else
          <a href="#" data-toggle="modal" data-target="#img_modal_{{$item->id}}" title="Photo">
            <i class="fa fa-fw fa-image"> </i>
          </a>
          @include('dashboard.components.imageModal', ['id' => $item->id,'img' => $item->image])
        @endif
      </td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.City') }}</strong></td>
      <td>{{ $item->City['name_'.App::getLocale()]}}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Category') }}</strong></td>
      <td>{{ $item->Category['name_'.App::getLocale()]}}</td>
    </tr>

      <tr>
          <td><strong>{{ __('dashboard.Average') }}</strong></td>
        <td>{{ $item->average }}</td>
      </tr>
    <tr>
      @php $images = \App\Image::where('store_id', $item->id)->count();@endphp
      <td><strong>{{ __('dashboard.Images') }}</strong></td>
      <td><a href="{{ route('admin.images.show', [App::getLocale(), $item->id]) }}">{{$images}}</a></td>
    </tr>
    <tr>
      @php $licenses = \App\License::where('store_id', $item->id)->count();@endphp
      <td><strong>{{ __('dashboard.Licenses') }}</strong></td>
      <td><a href="{{ route('admin.licenses.show', [App::getLocale(), $item->id]) }}">{{$licenses}}</a></td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Bank') }}</strong></td>
      <td>{{ $item->bank }}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Account') }}</strong></td>
      <td>{{ $item->account }}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Address') }}</strong></td>
      <td>{{ $item['address_'.App::getLocale()]}}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Minimum') }}</strong></td>
      <td>{{ $item->minimum }}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Expiration') }}</strong></td>
      <td>{{ $item->expiration }}</td>
    </tr>
    <tr>
      <td><strong>{{ __('dashboard.Status') }}</strong></td>
      <td>{{ $item->active == '0' ? __('dashboard.Not_Active') : __('dashboard.Active') }}</td>
    </tr>
  </table>

</div>
</div>

@endsection
