@php
    $headers = [
            $resource['header'] => '#',
        ];
    $tableCols = [
         __('dashboard.User'),
         __('dashboard.Value'),
         __('dashboard.Comment'),

       ];
@endphp
@extends('dashboard.layouts.app')
@section('title', __('dashboard.'.$resource['title']))
@section('content')
    @include('dashboard.components.header')
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-{{$resource['icon']}}"> </i> {{__('dashboard.'.$resource['header'])}}</h3>

                <div class="box-tools">
                    @include('dashboard.components.dangerModalMulti')
                </div>
            </div>
        <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                {!! Form::open(['method'=>'DELETE', 'route'=> [$resource['route'].'.multiDelete', App::getLocale()], 'class'=>'delete-form'])!!}
                    @if(count($data) == 0)
                        <div class="col-xs-12">
                            <h4> {{ __('dashboard.No Data') }}</h4>
                        </div>
                    @else
                        <table class="table table-hover">
                            <tr>
                                @foreach($tableCols as $col)
                                    <td><strong>{{ $col }}</strong></td>
                                @endforeach
                                <td><strong>{{__('dashboard.Actions')}}</strong></td>
                            </tr>
                            <br>
                            @foreach($data as $item)
                                <tr class="tr-{{ $item->id }}">
                                    <td>{{ $item->user['name'] }}</td>
                                    <td>{{ $item->value}}</td>
                                    <td>{{ $item->comment}}</td>
                                    <td>

                                         {{--<a href="{{ route($resource['route'].'.show', [App::getLocale(),$item->id]) }}" title="show"><i class="fa fa-fw fa-eye text-light-blue"></i></a>--}}
                                        <a href="{{ route($resource['route'].'.edit', [App::getLocale(), $item->id]) }}" title="edit"><i class="fa fa-fw fa-edit text-yellow"></i></a>
                                        <a href="#" data-toggle="modal" data-target="#danger_{{$item->id}}" title="Delete"><i class="fa fa-fw fa-trash text-red"></i></a>

                                    </td>
                                </tr>
                                @include('dashboard.components.dangerModal', ['user_name' => $item->name, 'id' => $item->id, 'resource' => $resource['route']])
{{--                                @include('dashboard.components.imageModal', ['id' => $item->id,'img' => $item->image])--}}
                             @endforeach
                        </table>
                    @endif
                {!! Form::close()!!}
            </div>
        </div>
    </div>
    <div class="text-center" >
        {{ $data->links() }}
    </div>
@endsection
