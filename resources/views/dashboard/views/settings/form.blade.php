<div class="form-group">
    <label for="banner" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__('dashboard.banner')}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::file('banner', array('id' => 'banner', 'multiple'=> 'multiple', 'class'=>'form-control', isset($item) ? '' : 'required'))!!}
    </div>
</div>

<div class="form-group">
    <label for="facebook" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.facebook")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::url('facebook', null, array('required', 'id' => 'facebook', 'placeholder'=>__('dashboard.facebook'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="instagram" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.instagram")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::url('instagram', null, array('required', 'id' => 'instagram', 'placeholder'=>__('dashboard.instagram'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="email" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.email")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::url('email', null, array('required', 'id' => 'email', 'placeholder'=>__('dashboard.email'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="twitter" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.twitter")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::url('twitter', null, array('required', 'id' => 'twitter', 'placeholder'=>__('dashboard.twitter'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="whatsapp" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.whatsapp")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::number('whatsapp', null, array('required', 'id' => 'whatsapp', 'placeholder'=>__('dashboard.whatsapp'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="phone" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.phone")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::number('phone', null, array('required', 'id' => 'phone', 'placeholder'=>__('dashboard.phone'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="about_ar" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.About_ar")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::textarea('about_ar', null, array('required', 'id' => 'about_ar', 'placeholder'=>__('dashboard.About_ar'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="about_en" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.About_en")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::textarea('about_en', null, array('required', 'id' => 'about_en', 'placeholder'=>__('dashboard.About_en'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="privacy_ar" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.Privacy_ar")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::textarea('privacy_ar', null, array('required', 'id' => 'privacy_ar', 'placeholder'=>__('dashboard.Privacy_ar'), 'class'=>'form-control'))!!}
    </div>
</div>

<div class="form-group">
    <label for="privacy_en" class="{{App::getLocale() == 'ar' ? 'col-md-push-10' : ''}} col-sm-2 control-label">{{__("dashboard.Privacy_en")}}</label>
    <div class="{{App::getLocale() == 'ar' ? 'col-md-pull-2' : ''}} col-sm-10">
        {!!Form::textarea('privacy_en', null, array('required', 'id' => 'privacy_en', 'placeholder'=>__('dashboard.Privacy_en'), 'class'=>'form-control'))!!}
    </div>
</div>


