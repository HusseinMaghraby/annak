<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cost')->default(0);
            $table->double('expected_cost')->default(0);
            $table->double('vat')->default(0);
            $table->double('discount')->default(0);
            $table->string('code')->nullable();
            $table->string('image')->nullable();
            $table->longText('desc')->nullable();
            $table->double('delivery_longitude')->default(0);
            $table->double('delivery_latitude')->default(0);
            $table->double('order_latitude')->default(0);
            $table->double('order_longitude')->default(0);
            $table->enum('payment', [1,2,3])->default(1);
            $table->enum('type', [1,2])->default(1);
            $table->enum('status', [1,2,3,4,5,6,7,8,9,10])->default(1);
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
