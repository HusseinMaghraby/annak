<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner')->nullable();
            $table->string('facebook');
            $table->string('instagram');
            $table->string('email');
            $table->string('twitter');
            $table->string('whatsapp');
            $table->string('phone');
            $table->longText('about_ar');
            $table->longText('about_en');
            $table->longText('privacy_ar');
            $table->longText('privacy_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
