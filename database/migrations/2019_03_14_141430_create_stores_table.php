<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('region_id')->nullable();
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image')->nullable();
            $table->string('phone')->nullable();
            $table->string('bank');
            $table->string('account');
            $table->string('address_ar')->nullable();
            $table->string('address_en')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('minimum')->nullable();
            $table->string('stars')->nullable();
            $table->string('rate')->nullable();
            $table->string('expiration')->nullable();
            $table->string('average')->nullable();
            $table->enum('active', [0,1])->default(0);
            $table->enum('status', [0,1])->default(0);
            $table->string('api_token')->unique();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
